//
//  ImageViewController.swift
//  FSTalkTest
//
//  Created by Innovation on 2021/04/05.
//

import Foundation
import UIKit
import AsyncImageManager

class ImageViewController:UIViewController {
    let imageManager = AsyncImageManager()
    var prevView: UIViewController?
    var stringUrl: String=""
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: self.stringUrl)
        
        self.imageManager.loadImage(imageView: imageView!, url: url!, successHandler: {data in
            
        }, failureHandler: {error in
            print(error)
        })
    }
}
