//
//  FSTalkModel.swift
//  FSTalkTest
//
//  Created by Innovation on 2021/04/02.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - FSTalkModel
struct FSTalkModel: Codable {
    let ogpList: [OgpList]
    let attachList: [AttachList]
    let itemList: [JSONAny]
    let uploadList: [UploadList]
    let readList, questionOptionList, questionAnswerList, groupInfoCountDic: [JSONAny]
    let infoCount: Int
    let postList: [PostList]
    let favoriteList: [FavoriteList]
    let group: Group

    enum CodingKeys: String, CodingKey {
        case ogpList = "ogp_list"
        case attachList = "attach_list"
        case itemList = "item_list"
        case uploadList = "upload_list"
        case readList = "read_list"
        case questionOptionList = "question_option_list"
        case questionAnswerList = "question_answer_list"
        case groupInfoCountDic = "group_info_count_dic"
        case infoCount = "info_count"
        case postList = "post_list"
        case favoriteList = "favorite_list"
        case group
    }
}

// MARK: - AttachList
struct AttachList: Codable {
    let id, postID, photoID, isDelete: Int
    let createdAt, updatedAt: String
    let videoID: Int

    enum CodingKeys: String, CodingKey {
        case id
        case postID = "post_id"
        case photoID = "photo_id"
        case isDelete = "is_delete"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case videoID = "video_id"
    }
}

// MARK: - FavoriteList
struct FavoriteList: Codable {
    let id, userID, postID, isCancel: Int
    let isDelete: Int
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case postID = "post_id"
        case isCancel = "is_cancel"
        case isDelete = "is_delete"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Group
struct Group: Codable {
    let id, type: Int
    let name: String
    let defaultRoleID, iconID, memberCount, isClose: Int
    let isDelete: Int
    let createdAt, updatedAt, groupDescription: String
    let latestPostID, postCount, priority: Int

    enum CodingKeys: String, CodingKey {
        case id, type, name
        case defaultRoleID = "default_role_id"
        case iconID = "icon_id"
        case memberCount = "member_count"
        case isClose = "is_close"
        case isDelete = "is_delete"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case groupDescription = "description"
        case latestPostID = "latest_post_id"
        case postCount = "post_count"
        case priority
    }
}

// MARK: - OgpList
struct OgpList: Codable {
    let id, postID, uploadID, mediaType: Int
    let title, ogpListDescription: String?
    let url: String
    let titleLineNum, isDelete: Int
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case postID = "post_id"
        case uploadID = "upload_id"
        case mediaType = "media_type"
        case title
        case ogpListDescription = "description"
        case url
        case titleLineNum = "title_line_num"
        case isDelete = "is_delete"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - PostList
struct PostList: Codable {
    let id, groupID, userID, parentID: Int
    let isComment: Int
    let text: String
    let postLineNum, commLineNum, isLink, itemID: Int
    let questionEndAt: JSONNull?
    let isMultiAnswer, isDelete, commentCount, favoriteCount: Int
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case groupID = "group_id"
        case userID = "user_id"
        case parentID = "parent_id"
        case isComment = "is_comment"
        case text
        case postLineNum = "post_line_num"
        case commLineNum = "comm_line_num"
        case isLink = "is_link"
        case itemID = "item_id"
        case questionEndAt = "question_end_at"
        case isMultiAnswer = "is_multi_answer"
        case isDelete = "is_delete"
        case commentCount = "comment_count"
        case favoriteCount = "favorite_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - UploadList
struct UploadList: Codable {
    let id, userID: Int
    let name: String
    let url: String
    let type, width, height, widthThumb: Int
    let heightThumb: Int
    let urlThumb: String?
    let widthSmall, heightSmall: Int
    let urlSmall: String?
    let widthMedium, heightMedium: Int
    let urlMedium: String?
    let isDelete: Int
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name, url, type, width, height
        case widthThumb = "width_thumb"
        case heightThumb = "height_thumb"
        case urlThumb = "url_thumb"
        case widthSmall = "width_small"
        case heightSmall = "height_small"
        case urlSmall = "url_small"
        case widthMedium = "width_medium"
        case heightMedium = "height_medium"
        case urlMedium = "url_medium"
        case isDelete = "is_delete"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
