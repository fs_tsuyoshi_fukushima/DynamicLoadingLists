//
//  WebAPICommunication.swift
//  FSLib
//
//  Created by Innovation on 2021/03/23.
//

import Foundation
import UIKit

open class WebAPICommunication: NSObject {
    public enum HTTPMethod: Int {
        case get, post
        
        func toString() -> String {
            return ["GET", "POST"][self.rawValue]
        }
    }
    
    public enum buildEnvironmentURL: String
    {
        case develop = "http://192.168.0.106:8000/"
        case release = ""
    }

    open var serverURL: String {
        #if DEBUG
            return buildEnvironmentURL.develop.rawValue
        #else
            return buildEnvironmentURL.release.rawValue
        #endif
    }
    
    open var isPostJson: Bool {
        return false
    }

    public typealias SuccessHandler = (_ data: Data) -> Void
    public typealias FailureHandler = (_ error: Error) -> Void
    public typealias requestParams = [String: Any]
    public let responseOK = 200
    
    fileprivate func toString(fromDictionary dictionary: requestParams, separator: String) -> String
    {
        let keyValueString: (String, Any) -> String = { (key, value) -> String in
            if let stringValue = value as? String
            {
                return key + "=" + stringValue
                
            }
            return key + "=\(value)"
        }
        
        var results = ""
        for key in dictionary.keys
        {
            if let value = dictionary[key]
            {
                if let array = value as? [AnyObject]
                {
                    // 配列の場合
                    let arrayKey = key + "[]"
                    for arrayItem in array
                    {
                        if results.isEmpty == false
                        {
                            results = results + separator
                        }
                        results += keyValueString(arrayKey, arrayItem)
                    }
                }
                else
                {
                    // 配列ではない
                    if results.isEmpty == false
                    {
                        results = results + separator
                    }
                    results += keyValueString(key, value)
                }
            }
        }
        return results
    }

    /// リクエストを作成する
    /// ※このメソッドではマルチパートのリクエストは作成できません
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - method: メソッド
    ///   - requestParams: リクエストパラメータ
    ///   - cachePolicy: キャッシュポリシー
    ///   - timeoutInterval: タイムアウト
    /// - Returns: 作成したリクエスト
    public func makeRequest(url:String, method: HTTPMethod, requestParams: requestParams, cachePolicy: URLRequest.CachePolicy, timeoutInterval: TimeInterval) -> URLRequest?
    {
        var request: URLRequest?
        
        // API共通のパラメータを追加する
        let params = requestParams
        let requestURL = URL(string: self.serverURL)!.appendingPathComponent(url)
        
        if (method == HTTPMethod.get)
        {
            // GETの場合
            // requestParams の情報を連結したクエリ文字列を URL に追加する
            // URLを http://〜/〜?xxxx=xxxx&xxxx=xxx の形式にする
            let query = self.toString(fromDictionary: params, separator: "&")
            let addQueryString: String = requestURL.absoluteString + "?" + query

            let requestURLAddQuery = URL(string: addQueryString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

            request = URLRequest(url: requestURLAddQuery!, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
            request?.httpMethod = method.toString()
        }
        else if (method == HTTPMethod.post)
        {
            // POSTの場合
            request = URLRequest(url: requestURL, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
            request?.httpMethod = method.toString()
            do {
                if (self.isPostJson == false)
                {
                    // 通常のPOST
                    request?.httpBody = self.toString(fromDictionary: params, separator: "&").data(using: String.Encoding.utf8)
                }
                else
                {
                    // JSON形式のPOST
                    request?.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                }
            }
            catch let error {
                print(error)
            }
        }
        
        return request
    }

    /// リクエストを送信する
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - method: メソッド
    ///   - requestParams: リクエストパラメータ
    ///   - cachePolicy: キャッシュポリシー
    ///   - timeoutInterval: タイムアウト
    ///   - successHandler: 成功時の処理
    ///   - failureHandler: エラー時の処理
    /// - Returns: 作成したリクエスト
    public func sendRequest(_ url: String, method: HTTPMethod, requestParams: requestParams, cachePolicy: URLRequest.CachePolicy, timeoutInterval: TimeInterval, successHandler: SuccessHandler?, failureHandler: FailureHandler?)
    {
        if let request = self.makeRequest(url: url, method: method, requestParams: requestParams, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
        {
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: { [unowned self] (data, response, error) -> Void in

                DispatchQueue.main.async(execute: { () -> Void in
                    if let httpResponse = response as? HTTPURLResponse
                    {
                        if httpResponse.statusCode == responseOK
                        {
                            // HTTPステータスが200
                            // HTTPステータスが200なのに data が nil(実際に発生したので対策してます)
                            if let data = data
                            {
                                // 正常
                                successHandler?(data)
                                return
                            }
                        }

                        // HTTPステータスが200以外 or data == nil
                        if let error = error {
                            failureHandler?(error)
                        }
                        else {
                            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "サーバーに接続できませんでした。"])
                            failureHandler?(errorResult)
                        }
                    }
                    else
                    {
                        if let error = error {
                            failureHandler?(error)
                        }
                        else {
                            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "サーバーに接続できませんでした。"])
                            failureHandler?(errorResult)
                        }
                        
                    }
                })
            })
            task.resume()
        }
        else
        {
            // リクエストが作成できなかった
            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "リクエストの作成に失敗しました。"])
            failureHandler?(errorResult)
        }
        
    }

    /// マルチパートで文字列を送信する際のパートを追加する
    ///
    /// - Parameters:
    ///   - paramKey: キー
    ///   - string: string文字列
    /// - Returns: 追加するパート
    private func addPartString (paramKey:String, string: String) -> Data {
        var body = Data()
        body.append("Content-Disposition: form-data; name=\"\(paramKey)\";\r\n\r\n".data(using: .utf8)!)
        body.append(string.data(using: .utf8)!)
        body.append("\r\n".data(using: .utf8)!)

        return body
    }
    
    /// マルチパートで画像を送信する際のパートを追加する
    ///
    /// - Parameters:
    ///   - paramKey: キー
    ///   - image: 画像
    /// - Returns: 追加するパート
    open func addPartImage (paramKey:String, image: UIImage) -> Data {
        var body = Data()
        let imageData = image.jpegData(compressionQuality: 1.0)
        body.append("Content-Disposition: form-data; name=\"\(paramKey)\"; filename=\"\(UUID().uuidString).jpg\"\r\n".data(using: .utf8)!)
        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
        body.append(imageData!)
        body.append("\r\n".data(using: .utf8)!)
        
        return body
    }

    /// マルチパートで画像を送信する際のパートを追加する
    ///
    /// - Parameters:
    ///   - paramKey: キー
    ///   - data: 送信するファイルデータ
    /// - Returns: 追加するパート
    open func addPartData (paramKey:String, data: Data) -> Data {
        var body = Data()
        body.append("Content-Disposition: form-data; name=\"\(paramKey)\"; filename=\"\(UUID().uuidString)\"\r\n".data(using: .utf8)!)
        body.append("Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8)!)
        body.append(data)
        body.append("\r\n".data(using: .utf8)!)
        
        return body
    }
    
    /// 文字列、画像、Data以外でマルチパート送信する際のパートを追加する
    ///
    /// - Parameters:
    ///   - paramKey: キー
    ///   - value: 送信するファイルデータ
    /// - Returns: 追加するパート
    open func addPartOther (paramKey:String, value: Any) -> Data {
        var body = Data()
        body.append("Content-Disposition: form-data; name=\"\(paramKey)\"\r\n\r\n".data(using: .utf8)!)
        body.append(String(describing: value).data(using: .utf8)!)
        body.append("\r\n".data(using: .utf8)!)
        
        return body
    }

    /// マルチパートで画像を送信する際のBody部を作成する
    ///
    /// - Parameters:
    ///   - paramKey: キー
    ///   - param: パラメータ
    /// - Returns: 追加するパート
    fileprivate func makeMultipartBody(paramKey: String, param: Any, boundary: String) -> Data? {
        var body = Data()
        let boundaryText = "--\(boundary)\r\n"
        body.append(boundaryText.data(using: .utf8)!)
        switch param
        {
        case let string as String:
            body.append(addPartString(paramKey: paramKey, string: string))
            
        case let image as UIImage:
            body.append(addPartImage(paramKey: paramKey, image: image))
            
        case let data as Data:
            body.append(addPartData(paramKey: paramKey, data: data))
            
        case let value as Any:
            body.append(addPartOther(paramKey: paramKey, value: value))

        default:
            return nil
        }

        return body
    }

    /// マルチパートのPOSTリクエストを作成する
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - cachePolicy: キャッシュポリシー
    ///   - timeoutInterval: タイムアウト
    ///   - requestParams: リクエストパラメータ
    ///   - addPartHandler: パート追加用のクロージャー(falseを返せば標準のパート追加処理が行われる)
    /// - Returns: 作成したリクエスト
    public func makeMultiPartRequest(_ url: String, cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy, timeoutInterval: TimeInterval, requestParams: requestParams) -> URLRequest?
    {
        // マルチパートのリクエストを作成する
        let requestURL = URL(string: self.serverURL)?.appendingPathComponent(url)
        var request = URLRequest(url: requestURL!, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
        request.httpMethod = HTTPMethod.post.toString()
        
        // バウンダリを作成する
        let uniqueId = UUID().uuidString
        let boundary = "---------------------------\(uniqueId)"
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        // マルチパートのBody部
        var multipartBody = Data()
        
        for paramKey in requestParams.keys
        {
            if let value = requestParams[paramKey]
            {
                if let array = value as? [AnyObject]
                {
                    for item in array
                    {
                        if let body = makeMultipartBody(paramKey: paramKey, param: item, boundary: boundary) {
                            multipartBody.append(body)
                        } else {
                            return nil
                        }
                    }
                }
                else
                {
                    if let body = makeMultipartBody(paramKey: paramKey, param: value, boundary: boundary) {
                        multipartBody.append(body)
                    } else {
                        return nil
                    }
                }
            }
        }
        multipartBody.append("--\(boundary)--\r\n".data(using: .utf8)!)
        
        request.httpBody = multipartBody
        
        return request
    }

    /// マルチパートでリクエストを送信する
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - method: メソッド
    ///   - requestParams: リクエストパラメータ
    ///   - cachePolicy: キャッシュポリシー
    ///   - timeoutInterval: タイムアウト
    ///   - successHandler: 成功時の処理
    ///   - failureHandler: エラー時の処理
    /// - Returns: 作成したリクエスト
    public func sendMultipartRequest(_ url: String, method: HTTPMethod, requestParams: requestParams, cachePolicy: URLRequest.CachePolicy, timeoutInterval: TimeInterval, successHandler: SuccessHandler?, failureHandler: FailureHandler?)
    {
        if let request = self.makeMultiPartRequest(url, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval, requestParams: requestParams) {
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: { [unowned self] (data, response, error) -> Void in

                DispatchQueue.main.async(execute: { () -> Void in
                    if let httpResponse = response as? HTTPURLResponse
                    {
                        if httpResponse.statusCode == responseOK
                        {
                            // HTTPステータスが200
                            // HTTPステータスが200なのに data が nil(実際に発生したので対策してます)
                            if let data = data
                            {
                                // 正常
                                successHandler?(data)

                                return
                            }
                        }

                        // HTTPステータスが200以外 or data == nil
                        if let error = error {
                            failureHandler?(error)
                        }
                        else {
                            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "サーバーに接続できませんでした。"])
                            failureHandler?(errorResult)
                        }
                    }
                    else
                    {
                        if let error = error {
                            failureHandler?(error)
                        }
                        else {
                            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "サーバーに接続できませんでした。"])
                            failureHandler?(errorResult)
                        }
                        
                    }
                })
            })
            task.resume()
        }
        else
        {
            // リクエストが作成できなかった
            let errorResult = NSError(domain: "", code: 0, userInfo: ["description": "リクエストの作成に失敗しました。"])
            failureHandler?(errorResult)
        }
    }
}
