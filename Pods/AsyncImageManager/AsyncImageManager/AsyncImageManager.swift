//
//  AsyncImageManager.swift
//  AsyncImageManager
//
//  Created by Innovation on 2021/04/01.
//

import Foundation
import UIKit
import Kingfisher

open class AsyncImageManager: UIImageView {
    static let sharedAsyncImageManager = AsyncImageManager()
    // Kingfisher のキャッシュ
    public let cache = ImageCache.default
    public var maximumRetryCount = 5
    public var retryIntervalSec = 1
    public var maximumRequestCount = 5
    private var requestCount = 0
    public var errorImage = UIImage(named: "error")
    // セマフォでもリクエスト数の制限をかけることも可能だが、Kingfisher側でもセマフォによって制御している
//    public var semaphoreValue = 2
//    let semaphore = DispatchSemaphore(value: 2)

    /// キャッシュに登録する画像サイズの最大値（縦横とも0：無制限）
    public var maximumCacheImageSize = CGSize(width: 0.0, height: 0.0)

    public typealias SuccessHandler = (_ result: RetrieveImageResult) -> Void
    public typealias FailureHandler = (_ error: Error) -> Void
        
    public func cancel(imageView: UIImageView) {
        imageView.kf.cancelDownloadTask()
    }
    
    public func loadImage(imageView: UIImageView, url: URL, successHandler: @escaping SuccessHandler, failureHandler: @escaping FailureHandler)
    {
        // インジケータ表示
        imageView.kf.indicatorType = .activity
        
        let retry = DelayRetryStrategy(maxRetryCount: maximumRetryCount, retryInterval: .seconds(TimeInterval(retryIntervalSec)))
//        let resizeProcessor = ResizingImageProcessor(referenceSize: imageView.frame.size)
//        let resizeProcessor = DownsamplingImageProcessor(size: imageView.frame.size)
//        , .processor(resizeProcessor)
        imageView.kf.setImage(with: url, options: [.retryStrategy(retry)]) { result in
            // `result` is either a `.success(RetrieveImageResult)` or a `.failure(KingfisherError)`
//            DispatchQueue.global().async(execute: { () -> Void in
//                defer {
//                    print("signal")
//                    self.semaphore.signal()
//                }
//                print("wait")
//                self.semaphore.wait()
            
                switch result {
                case .success(let value):
                    // The image was set to image view:
                    // 画像サイズ（横）の最大値を超えていないかチェック
                    if ((self.maximumCacheImageSize.width > 0.0 && value.image.size.width > self.maximumCacheImageSize.width)
                    || (self.maximumCacheImageSize.height > 0.0 && value.image.size.height > self.maximumCacheImageSize.height))
                    {
                        if self.cache.isCached(forKey: url.absoluteString) {
                            self.cache.removeImage(forKey: url.absoluteString)
                        }
                    }
                    successHandler(value)
                case .failure(let error):
                    //エラー画像設定
                    imageView.kf.base.image = self.errorImage
                    failureHandler(error)
                }
        }
    }
}
