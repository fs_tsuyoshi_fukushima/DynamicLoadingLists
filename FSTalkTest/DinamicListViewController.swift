//
//  DinamicListViewController.swift
//  FSTalkTest
//
//  Created by Innovation on 2021/04/01.
//

import Foundation
import UIKit
import AsyncImageManager
import WebAPICommunication
import LoadMoreTableViewController

struct ItemData {
    var imageUrl: String?
    var text: String
}

class DinamicListViewController: LoadMoreTableViewController, UIAdaptivePresentationControllerDelegate {
    let imageManager = AsyncImageManager()
    let webAPICom = SubWebAPICommunication()
    var page: Int = 0
    var isLoading = false
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cellReuseIdentifier = "TableViewCell"
        tableView.dataSource = self
        tableView.delegate = self
        tableView.prefetchDataSource = self

        fetchSourceObjects = { [weak self] completion in
            self?.fetchData(page: self!.page, completion: { data in
                var array: [ItemData] = []
                let postList = data.postList

                for post in postList {
                    if (post.parentID != 0) {
                        continue
                    }
                    
                    let upload: UploadList
                    
                    if let attach = data.attachList.first(where: { e in e.postID == post.id }) {
                        upload = data.uploadList.first(where: { e in e.id == attach.photoID })!
                        let listData: ItemData! = ItemData(imageUrl: upload.url, text: post.text)
                        array.append(listData)
                    } else {
                        let listData: ItemData! = ItemData(imageUrl: nil, text: post.text)
                        array.append(listData)
                    }
                }
                completion(array, true)
            })
        }
        
        configureCell = { [weak self] cell, row in
            let data = self?.sourceObjects as! [ItemData]
            if let imageUrl = data[row].imageUrl {
                let url = URL(string: imageUrl)
                let imageView = cell.imageView!
                self?.imageManager.loadImage(imageView: imageView, url: url!, successHandler: {data in
                    let resizeImage = self?.cropThumbnailImage(image: data.image, w: 100, h: 100)
                    cell.imageView?.image = resizeImage
                    
                }, failureHandler: {error in
                    print(error)
                })
            }
            cell.textLabel?.text = data[row].text
            
            return cell
        }
        
        // タップ時処理
        didSelectRow = { row in
            let data = self.sourceObjects as! [ItemData]
            if let imageUrl = data[row].imageUrl {
                let cu = ImageViewController()
                cu.prevView = self
                cu.presentationController?.delegate = self
                cu.stringUrl = imageUrl
            }
        }
    }
    
    // APIを叩く
    private func fetchData(page: Int, completion: @escaping (FSTalkModel) -> Void) {
        let param = [
            "user_id":"28",
            "page":String(page),
            "group_id":"80",
            "read_post_id":"-1",
            "build_version":"20211218_151839",
            "session_user_id":"28"
        ]
        webAPICom.sendRequest("api/post/group_list", method: .post, requestParams: param, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 20, successHandler: { (data) in
            print("Result")
            do {
                self.page += 1
                let fstalk = try JSONDecoder().decode(FSTalkModel.self, from: data)
//                var str: String = String.init(data: data, encoding: .utf8)!
                completion(fstalk)
            } catch let error {
                print("decode error")
                print(error)
            }
        }, failureHandler: {(error) in
            print("ERROR")
            print(error)
        })
    }
    
    //　画像リサイズ処理
    private func cropThumbnailImage(image :UIImage, w:Int, h:Int) ->UIImage
    {
        let origRef    = image.cgImage
        let origWidth  = Int(origRef!.width)
        let origHeight = Int(origRef!.height)
        var resizeWidth:Int = 0, resizeHeight:Int = 0

        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
        }

        let resizeSize = CGSize.init(width: CGFloat(resizeWidth), height: CGFloat(resizeHeight))

        UIGraphicsBeginImageContext(resizeSize)

        image.draw(in: CGRect.init(x: 0, y: 0, width: CGFloat(resizeWidth), height: CGFloat(resizeHeight)))

        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        // 切り抜き処理

        let cropRect  = CGRect.init(x: CGFloat((resizeWidth - w) / 2), y: CGFloat((resizeHeight - h) / 2), width: CGFloat(w), height: CGFloat(h))
        let cropRef   = resizeImage!.cgImage!.cropping(to: cropRect)
        let cropImage = UIImage(cgImage: cropRef!)

        return cropImage
    }
}

class SubWebAPICommunication: WebAPICommunication {
    override var serverURL: String {
        return "https://5dpgpzfaok.execute-api.ap-northeast-1.amazonaws.com/"
    }
}

extension DinamicListViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths == [[1,0]] {
            print(#function + " \(indexPaths)")
            loadMore()
        }
    }
}
